const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const config = {
    mode: 'development',
    entry: path.resolve(__dirname, 'index.js'),
    output:{
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js'
    },
    module: {
        rules: [
            // Aquí van los loaders
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    //'style-loader',
                    'css-loader'
                ],
            }
        ]
    },
    plugins: [
        // Aquí van los plugins
        new MiniCssExtractPlugin({
            filename: "css/[name].css",
            chunkFilename: "[id].css"
        })
    ]
}

module.exports = config